﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BlackPlain.NuGet.Manager.NuGet
{
    [XmlRoot("entry")]
    public class Package
    {
        [XmlElement("id")]
        public string Id { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("summary")]
        public string Summary { get; set; }

        [XmlElement("updated")]
        public DateTime Updated { get; set; }

        [XmlElement("author")]
        public Author Author { get; set; }

        [XmlElement("link")]
        public List<Link> Links { get; set; }

        [XmlElement("category")]
        public Category Category { get; set; }

        [XmlElement("content")]
        public Content Content { get; set; }

        [XmlElement("properties", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata")]
        public Properties Properties { get; set; }
    }
}