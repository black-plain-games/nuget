﻿using System.Xml.Serialization;

namespace BlackPlain.NuGet.Manager.NuGet
{
    [XmlRoot("content")]
    public class Content
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("src")]
        public string Location { get; set; }
    }
}