﻿using System.Xml.Serialization;

namespace BlackPlain.NuGet.Manager.NuGet
{
    [XmlRoot("category")]
    public class Category
    {

        [XmlAttribute("term")]
        public string Term { get; set; }

        [XmlAttribute("scheme")]
        public string Scheme { get; set; }
    }
}