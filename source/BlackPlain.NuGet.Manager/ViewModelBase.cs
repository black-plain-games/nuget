﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BlackPlain.NuGet.Manager
{
    public class ViewModelBase : NotifyOnPropertyChanged
    {
        public bool IsBusy
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        protected ViewModelBase()
        {
            _commands = new Dictionary<Action<object>, IDictionary<Predicate<object>, ICommand>>();
        }

        protected string GetAppSetting([CallerMemberName]string key = "")
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException(nameof(key));
            }
            return ConfigurationManager.AppSettings[key];
        }

        protected ICommand GetCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException(nameof(execute));
            }

            if (canExecute == null)
            {
                throw new ArgumentNullException(nameof(canExecute));
            }

            if (!_commands.ContainsKey(execute))
            {
                _commands.Add(execute, new Dictionary<Predicate<object>, ICommand>());
            }

            var candidates = _commands[execute];

            ICommand output;

            if (!candidates.ContainsKey(canExecute))
            {
                Debug.WriteLine("Creating new RelayCommand");

                output = new RelayCommand(execute, canExecute);

                candidates.Add(canExecute, output);
            }
            else
            {
                output = candidates[canExecute];
            }

            return output;
        }

        protected ICommand GetCommand(Action execute, Predicate<object> canExecute)
        {
            return GetCommand(x => execute(), canExecute);
        }

        protected ICommand GetCommand(Action<object> execute)
        {
            return GetCommand(execute, NullPredicate);
        }

        protected ICommand GetCommand(Action execute)
        {
            return GetCommand(x => execute(), NullPredicate);
        }

        protected ICommand GetAsyncCommand(Action<object> execute, Predicate<object> canExecute, bool isBusy)
        {
            return GetCommand(x => ExecuteAsync(x, execute, isBusy), canExecute);
        }

        protected ICommand GetAsyncCommand(Action<object> execute, Predicate<object> canExecute)
        {
            return GetCommand(x => ExecuteAsync(x, execute, false), canExecute);
        }

        protected ICommand GetAsyncCommand(Action execute, Predicate<object> canExecute, bool isBusy)
        {
            return GetAsyncCommand(x => execute(), canExecute, isBusy);
        }

        protected ICommand GetAsyncCommand(Action execute, Predicate<object> canExecute)
        {
            return GetAsyncCommand(x => execute(), canExecute);
        }

        protected ICommand GetAsyncCommand(Action<object> execute, bool isBusy)
        {
            return GetAsyncCommand(execute, NullPredicate, isBusy);
        }

        protected ICommand GetAsyncCommand(Action<object> execute)
        {
            return GetAsyncCommand(execute, NullPredicate);
        }

        protected ICommand GetAsyncCommand(Action execute, bool isBusy)
        {
            return GetAsyncCommand(x => execute(), NullPredicate, isBusy);
        }

        protected ICommand GetAsyncCommand(Action execute)
        {
            return GetAsyncCommand(x => execute(), NullPredicate);
        }

        protected bool NullPredicate(object data)
        {
            return true;
        }

        private void ExecuteAsync(object data, Action<object> execute, bool isBusy)
        {
            if (isBusy)
            {
                IsBusy = true;
            }

            Task.Factory.StartNew(() =>
            {
                execute(data);

                if (isBusy)
                {
                    IsBusy = false;
                }
            });
        }

        private readonly IDictionary<Action<object>, IDictionary<Predicate<object>, ICommand>> _commands;
    }
}