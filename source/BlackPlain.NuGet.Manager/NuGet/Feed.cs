﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BlackPlain.NuGet.Manager.NuGet
{
    [XmlRoot("feed", Namespace = "http://www.w3.org/2005/Atom")]
    public class Feed
    {
        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("id")]
        public string Id { get; set; }

        [XmlElement("updated")]
        public DateTime Update { get; set; }

        [XmlElement("entry")]
        public List<Package> Packages { get; set; }
    }
}