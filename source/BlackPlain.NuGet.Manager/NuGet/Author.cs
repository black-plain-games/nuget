﻿using System.Xml.Serialization;

namespace BlackPlain.NuGet.Manager.NuGet
{
    [XmlRoot("author")]
    public class Author
    {
        [XmlElement("name")]
        public string Name { get; set; }
    }
}