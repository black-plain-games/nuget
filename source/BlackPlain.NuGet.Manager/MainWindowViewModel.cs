﻿using BlackPlain.NuGet.Manager.NuGet;
using BlackPlain.Wpf.Core;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;

namespace BlackPlain.NuGet.Manager
{
    public class MainWindowViewModel : ViewModelBase
    {
        public Package SelectedPackage
        {
            get
            {
                return GetField<Package>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand RefreshFeedCommand => GetAsyncCommand(RefreshFeed, true);

        public ICommand DeletePackageCommand => GetAsyncCommand(DeletePackage, true);

        public ICommand UploadPackageCommand => GetAsyncCommand(UploadPackage, true);

        public ICommand DownloadPackageCommand => GetAsyncCommand(DownloadPackage, true);

        public Feed FeedData
        {
            get
            {
                return GetField<Feed>();
            }
            set
            {
                SetField(value);
            }
        }

        public IDictionary<string, IEnumerable<Package>> Packages
        {
            get
            {
                return GetField<IDictionary<string, IEnumerable<Package>>>();
            }
            set
            {
                SetField(value);
            }
        }

        protected void RefreshFeed()
        {
            var nugetUri = new Uri(NugetUrl);

            var packagesUrl = new Uri(nugetUri, "nuget/Packages");

            var request = (HttpWebRequest)WebRequest.Create(packagesUrl);

            using (var response = request.GetResponse())
            using (var responseStream = response.GetResponseStream())
            {
                var xmlSerializer = new XmlSerializer(typeof(Feed));

                var p = responseStream.ToString();

                FeedData = (Feed)xmlSerializer.Deserialize(responseStream);
            }

            Packages = FeedData.Packages.GroupBy(p => p.Title).ToDictionary(g => g.Key, g => g.OrderByDescending(p => p.Properties.Version).AsEnumerable());
        }

        protected void DeletePackage()
        {
            var confirmDeletionResult = MessageBox.Show($"Are you sure you want to delete {SelectedPackage.Title} (Version {SelectedPackage.Properties.Version})?", "Confirm Deletion", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (confirmDeletionResult != MessageBoxResult.Yes)
            {
                return;
            }

            var nugetPath = Path.Combine(NugetExePath, "nuget");

            var parameters = $"delete {SelectedPackage.Title} {SelectedPackage.Properties.Version} {NugetApiKey} -Source {NugetUrl}";

            var process = new Process
            {
                StartInfo = new ProcessStartInfo(nugetPath, parameters)
                {
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };

            process.Start();

            while (!process.HasExited)
            {
            }

            RefreshFeed();
        }

        protected void UploadPackage()
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "NuGet Package File|*.nupkg",
                Title = "Select package(s) to upload...",
                Multiselect = true
            };

            var openFileResult = openFileDialog.ShowDialog();

            if (!openFileResult.HasValue || !openFileResult.Value)
            {
                return;
            }

            var nugetPath = Path.Combine(NugetExePath, "nuget");

            foreach (var fileName in openFileDialog.FileNames)
            {
                var parameters = $"push {fileName} -source {NugetUrl} {NugetApiKey}";

                var process = new Process
                {
                    StartInfo = new ProcessStartInfo(nugetPath, parameters)
                    {
                        RedirectStandardOutput = true,
                        UseShellExecute = false,
                        CreateNoWindow = true
                    }
                };

                process.Start();

                while (!process.HasExited)
                {
                }
            }

            RefreshFeed();
        }

        protected void DownloadPackage()
        {
            string errorMessage = null;

            if (SelectedPackage.Content == null)
            {
                errorMessage = "no content was provided";
            }

            if (SelectedPackage.Content.Type != "application/zip")
            {
                errorMessage = $"content type was '{SelectedPackage.Content.Type}' but was supposed to be 'application/zip'";
            }

            if (string.IsNullOrWhiteSpace(SelectedPackage.Content.Location))
            {
                errorMessage = "no location was provided for the content";
            }

            if (errorMessage != null)
            {
                MessageBox.Show($"Cannot download {SelectedPackage.Title} (Version {SelectedPackage.Properties.Version}) because {errorMessage}.", "Download Failed", MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }

            var saveFileDialog = new SaveFileDialog
            {
                Title = "Save package...",
                Filter = "Zip File|*.zip"
            };


            var saveFileResult = saveFileDialog.ShowDialog();

            if (!saveFileResult.HasValue || !saveFileResult.Value)
            {
                return;
            }

            FileStream saveFileStream = null;

            var fileName = saveFileDialog.FileName;

            if (!fileName.EndsWith(".zip", StringComparison.Ordinal))
            {
                fileName += ".zip";
            }

            try
            {
                if (!File.Exists(fileName))
                {
                    saveFileStream = File.Create(fileName, (int)Math.Min(SelectedPackage.Properties.PackageSize, int.MaxValue));
                }
                else
                {
                    saveFileStream = File.OpenWrite(fileName);
                }

                var request = (HttpWebRequest)WebRequest.Create(SelectedPackage.Content.Location);

                using (var response = request.GetResponse())
                using (var responseStream = response.GetResponseStream())
                {
                    responseStream.CopyTo(saveFileStream);
                }
            }
            catch (Exception err)
            {
                errorMessage = err.Message;
            }
            finally
            {
                if (saveFileStream != null)
                {
                    saveFileStream.Dispose();

                    if (errorMessage != null)
                    {
                        File.Delete(fileName);
                    }
                }
            }

            if (errorMessage != null)
            {
                MessageBox.Show($"Failed to download {SelectedPackage.Title} (Version {SelectedPackage.Properties.Version}) because {errorMessage}.", "Download Failed", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        protected string NugetUrl => GetAppSetting();

        protected string NugetExePath => GetAppSetting();

        protected string NugetApiKey => GetAppSetting();
    }
}