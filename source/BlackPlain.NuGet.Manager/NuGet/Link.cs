﻿using System;
using System.Xml.Serialization;

namespace BlackPlain.NuGet.Manager.NuGet
{
    [XmlRoot("link")]
    public class Link
    {
        [XmlAttribute("rel")]
        public string Relationship { get; set; }

        [XmlAttribute("title")]
        public string Title { get; set; }

        [XmlAttribute("href")]
        public string Location { get; set; }
    }
}