﻿using BlackPlain.Wpf.Core;
using System.Windows;

namespace BlackPlain.NuGet.Manager
{
    public partial class MainWindow : Window
    {
        protected MainWindowViewModel ViewModel
        {
            get
            {
                return DataContext as MainWindowViewModel;
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            UIManager.SetUI();

            DataContext = new MainWindowViewModel();
        }
    }
}