﻿using System;
using System.Xml.Serialization;

namespace BlackPlain.NuGet.Manager.NuGet
{
    [XmlRoot("propeties")]
    public class Properties
    {
        [XmlElement("Version", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string Version { get; set; }

        [XmlElement("IsPrerelease", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public bool IsPrerelease { get; set; }

        [XmlElement("Title", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string Title { get; set; }

        [XmlElement("Owners", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string Owners { get; set; }

        [XmlElement("IconUrl", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string IconUrl { get; set; }

        [XmlElement("LicenseUrl", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string LicenseUrl { get; set; }

        [XmlElement("ProjectUrl", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string ProjectUrl { get; set; }

        [XmlElement("DownloadCount", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public int DownloadCount { get; set; }

        [XmlElement("RequireLicenseAcceptance", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public bool RequireLicenseAcceptance { get; set; }

        [XmlElement("DevelopmentDependency", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public bool DevelopmentDependency { get; set; }

        [XmlElement("Description", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string Description { get; set; }

        [XmlElement("ReleaseNotes", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string ReleaseNotes { get; set; }

        [XmlElement("Published", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public DateTime Published { get; set; }
        
        [XmlElement("Dependencies", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string Dependencies { get; set; }

        [XmlElement("PackageHash", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string PackageHash { get; set; }

        [XmlElement("PackageHashAlgorithm", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string PackageHashAlgorithm { get; set; }

        [XmlElement("PackageSize", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public long PackageSize { get; set; }

        [XmlElement("Copyright", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string Copyright { get; set; }

        [XmlElement("Tags", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string Tags { get; set; }

        [XmlElement("IsAbsoluteLatestVersion", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public bool IsAbsoluteLatestVersion { get; set; }

        [XmlElement("IsLatestVersion", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public bool IsLatestVersion { get; set; }

        [XmlElement("Listed", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public bool Listed { get; set; }

        [XmlElement("VersionDownloadCount", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public int VersionDownloadCount { get; set; }

        [XmlElement("MinClientVersion", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string MinClientVersion { get; set; }

        [XmlElement("Summary", Namespace = "http://schemas.microsoft.com/ado/2007/08/dataservices")]
        public string Summary { get; set; }
    }
}